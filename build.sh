# This is a shell script that runs the necessary commands to build the 
# Docker image.
docker pull debian:bullseye-slim
docker build -t reform-docker .
