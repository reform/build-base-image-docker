# Create a docker image containing a bunch of dependencies for 
# building the Reform system img file.

FROM debian:bullseye-slim
RUN sed -i 's|http://deb.|http://ftp.de.|g' /etc/apt/sources.list
RUN apt -y update
RUN apt-get -y install 
RUN apt-get -y install sudo \
                       gzip \
                       libext2fs2 \
                       pigz \
                       bc \
                       parted \
                       multistrap \
                       udisks2 \
                       gcc-aarch64-linux-gnu \
                       make \
                       device-tree-compiler \
                       qemu-user-static \
                       binfmt-support \
                       build-essential \
                       bison \
                       flex \
                       libssl-dev \
                       bash \
                       qemu-system-aarch64 \
                       git
